First written on 23/12/2018 by Partharaj Deb

#Dependencies

    dependencies {
        ***
        implementation 'com.android.volley:volley:1.1.1'
        implementation 'com.google.code.gson:gson:2.8.5'
        ***
    }

#Sample

    new NetRequestManager.Builder(this, true, 30 * 60 * 1000)
        .setEndPoint(endpoint)
        .useBasicAuth(true)
        .setHeader(headersMap)
        .setParam(parmsMap)
        .setOnResponse(new NetRequestManager.OnResponse() {
            @Override
            public void onSuccess(String response) {
                // TODO on success
            }

            @Override
            public void onError(NetRequestManager.Error error) {
                // TODO on error
            }
        })
        .build()
        .post();
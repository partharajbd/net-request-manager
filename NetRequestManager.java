package com.partharaj

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.partharaj.App;
import com.partharaj.BuildConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class NetRequestManager {

    private String TAG = getClass().getSimpleName();
    private String endPoint;
    private boolean useCookie;
    private Map<String, String> headers = new HashMap<>();
    private Map<String, String> params = new HashMap<>();
    private OnResponse onResponse;
    private Context context;
    private boolean isCacheEnabled = false;
    private CacheManager cacheManager;
    private boolean useBasicAuth;
    private boolean clearCache = false;

    public static class Builder {

        NetRequestManager netRequestManager;

        public Builder(Context context) {
            netRequestManager = new NetRequestManager();
            netRequestManager.context = context;
        }

        public Builder(Context context, boolean isCacheEnabled) {
            netRequestManager = new NetRequestManager();
            netRequestManager.context = context;
            netRequestManager.isCacheEnabled = isCacheEnabled;
            netRequestManager.cacheManager = new CacheManager(context);
        }

        public Builder(Context context, boolean isCacheEnabled, long cacheValidityInMillis) {
            netRequestManager = new NetRequestManager();
            netRequestManager.context = context;
            netRequestManager.isCacheEnabled = isCacheEnabled;
            netRequestManager.cacheManager = new CacheManager(context, cacheValidityInMillis);
        }

        public Builder setEndPoint(String endPoint) {
            netRequestManager.endPoint = endPoint;
            return this;
        }

        public Builder setHeader(@NonNull Map<String, String> headers) {
            netRequestManager.headers = headers;
            return this;
        }

        public Builder setParam(@NonNull Map<String, String> params) {
            netRequestManager.params = params;
            return this;
        }

        public Builder setOnResponse(OnResponse onResponse) {
            netRequestManager.onResponse = onResponse;
            return this;
        }

        public Builder useCookie(boolean useCookie) {
            netRequestManager.useCookie = useCookie;
            return this;
        }

        public Builder useBasicAuth(boolean useBasicAuth) {
            netRequestManager.useBasicAuth = useBasicAuth;
            return this;
        }

        public Builder clearCacheOnSuccess(boolean clearCache) {
            netRequestManager.clearCache = clearCache;
            return this;
        }

        public NetRequestManager build() {
            return netRequestManager;
        }

    }

    public void post() {
        sendRequest(Request.Method.POST);
    }

    public void get() {
        if (isCacheEnabled && cacheManager != null && cacheManager.isCacheAvailable(endPoint)) {
            if (onResponse != null) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onResponse.onSuccess(cacheManager.getCacheResponse(endPoint));
                    }
                }, 300);
                return;
            }
        }
        sendRequest(Request.Method.GET);
    }

    private void sendRequest(int method) {
        if (!noInternetErrorHandler()) {
            return;
        }
        Response.Listener onSuccess = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.log(TAG, "Success Response for: " + endPoint);
                Logger.log(TAG, "Response body: " + response);

                if (clearCache) {
                    cacheManager.clearCache();
                }
                if (isCacheEnabled && cacheManager != null && (response!= null && !response.equals("")))
                    cacheManager.storeCache(endPoint, response);

                if (onResponse != null) {
                    onResponse.onSuccess(response);
                }
            }
        };

        Response.ErrorListener onError = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Error netError = new Error();
                netError.setMessage(error.getMessage());
                if (error.networkResponse != null) {
                    netError.setStatus(error.networkResponse.statusCode);
                    if (error.networkResponse.data != null) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            netError.setBody(new String(error.networkResponse.data, StandardCharsets.UTF_8));
                        } else {
                            netError.setBody(new String(error.networkResponse.data));
                        }
                    }
                    Logger.log(TAG, "Error response for: " + endPoint);
                    Logger.log(TAG, "statusCode: " + netError.getStatus() + ", errorBody: " + netError.getBody());
                }
                if (onResponse != null) onResponse.onError(netError);

                unauthorizedHandler(netError);
            }

            private void unauthorizedHandler(Error netError) {
                if (netError.getStatus() == 401) {
                    JsonObject body = new JsonParser().parse(netError.body).getAsJsonObject();
                    if (body.has("message") && body.get("message").getAsString().equals("Unauthenticated.")) {
                        Toast.makeText(context, "Please login again.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };

        StringRequest request = new StringRequest(method, endPoint, onSuccess, onError) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                headers.put("accept", "application/json");

                if (useBasicAuth) {
                    String authToken = getAuthToken();
                    if (authToken != null && !authToken.isEmpty())
                        headers.put("Authorization", "Bearer " + authToken);
                }

                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(20000, 3, 1.0f));
        App.getApp().addToRequestQueue(request);
    }

    private boolean noInternetErrorHandler() {
        if (!isNetworkAvailable()) {
            new AlertDialog.Builder(context)
                    .setTitle("Offline")
                    .setMessage("Please connect to the internet.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setNegativeButton("Settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            context.startActivity(new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS));
                        }
                    }).show();
            return false;
        }
        return true;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = null;
        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
        }

        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public interface OnResponse {
        void onSuccess(String response);

        void onError(Error error);
    }

    private static class CacheManager {

        private long cacheValidityMillis = 3600000L;
        SharedPreferences sp;

        CacheManager(Context context) {
            sp = context.getSharedPreferences("NetRequestManager_CacheManager", Context.MODE_PRIVATE);
        }

        CacheManager(Context context, long cacheValidityMillis) {
            this.cacheValidityMillis = cacheValidityMillis;
            sp = context.getSharedPreferences("NetRequestManager_CacheManager", Context.MODE_PRIVATE);
        }

        boolean isCacheAvailable(String endpoint) {
            String response = sp.getString(endpoint, null);
            long validity = sp.getLong(endpoint + "validity", -1);

            if (response == null || validity < System.currentTimeMillis()) {
                sp.edit()
                        .remove(endpoint)
                        .remove(endpoint + "validity")
                        .apply();
                return false;
            }
            return true;
        }

        String getCacheResponse(String endpoint) {
            String response = sp.getString(endpoint, null);
            Logger.log("Cache Response", response);
            return response;
        }

        void storeCache(String endpoint, String response) {
            sp.edit()
                    .putString(endpoint, response)
                    .putLong(endpoint + "validity", System.currentTimeMillis() + cacheValidityMillis)
                    .apply();
        }

        void clearCache(){
            sp.edit().clear().apply();
        }
    }

    public void setAuthToken(String authToken) {
        context.getSharedPreferences("NetRequestManager_authPreference", Context.MODE_PRIVATE).edit().putString("AUTH_TOKEN", authToken).apply();
    }

    public String getAuthToken() {
        return context.getSharedPreferences("NetRequestManager_authPreference", Context.MODE_PRIVATE).getString("AUTH_TOKEN", null);
    }

    @SuppressWarnings("WeakerAccess")
    public class Error {
        private int status;
        private String body;
        private String message;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void toastCustomMessage() {
            if (body != null) {
                try {
                    JSONObject root = new JSONObject(body);
                    if (root.has("message")) {
                        Toast.makeText(context, root.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    //e.printStackTrace();
                }
            }
        }
    }

    private static class Logger {
        public static void log(Class c, String message) {
            if (BuildConfig.DEBUG) {
                Log.e(c.getClass().getName(), message);
            }
        }

        public static void log(String tag, String message) {
            if (BuildConfig.DEBUG) {
                Log.e(tag, message);
            }
        }

        public static void log(String tag, String message, Throwable tr) {
            if (BuildConfig.DEBUG) {
                Log.e(tag, message);
            }
        }

        public static void log(String message) {
            if (BuildConfig.DEBUG) {
                Log.e("com.partharaj", message);
            }
        }

        public static void logDump(String message) {
            logDump("", message);
        }

        public static void logDump(String tag, String message) {
            if (BuildConfig.DEBUG) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String now = sdf.format(new Date());
                    message = now + " " + tag + "\n" + message + "\n\n=====================================\n\n";

                    FileOutputStream os = new FileOutputStream(new File(Environment.getExternalStorageDirectory(), "dmcLog.txt"), true);
                    os.write(message.getBytes(), 0, message.length());
                    os.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
